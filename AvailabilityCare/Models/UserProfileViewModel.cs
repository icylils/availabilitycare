﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AvailabilityCare.Models
{
    public class UserProfileViewModel
    {
        public string EmailAddress { get; set; }

        public string Name { get; set; }

        public string ProfileImage { get; set; }
    }
}
