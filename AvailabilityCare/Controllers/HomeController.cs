﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AvailabilityCare.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace AvailabilityCare.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //try
            //{
            //    if (User.Identity.IsAuthenticated)
            //    {
            //        string accessToken = await HttpContext.GetTokenAsync("access_token");
            //        string idToken = await HttpContext.GetTokenAsync("id_token");
            //        Debug.WriteLine($"accessToken : {accessToken} idToken : {idToken}");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine($"IndexAsync Exception: {ex.Message} ");
            //}

            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult Map()
        {
            return View();
        }

        public IActionResult Chat()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public IActionResult Admin()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "About Us";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
